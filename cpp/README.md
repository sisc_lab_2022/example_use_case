# C++ part

## Description
In this part the network is read from json, pruned, and then written back. The paths need to be adjusted for that in main.cpp.


## Setup
The dependencies for the interval_network library are boost, Eigen3, nlohmann_json and NAG::dco_cpp. To install the first three ones (sudo previledges required):

    sudo apt-get install build-essential libboost-all-dev libeigen3-dev nlohmann-json3-dev

To set up the interval_network library, create a folder *extern* and clone the repository into it

    mkdir extern && cd extern && git clone https://git.rwth-aachen.de/sisc_lab_2022/interval_network.git

The dco library should also go into the extern folder. dco will be found if it is in the extern folder and is called dco (without the version number). Alternatively, dco can also be installed properly on the computer. In this case it will be located outomatically