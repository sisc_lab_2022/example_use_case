# Python part

## Description
the file train_simple_network.py is meant to serve as an example for training a network and the converting it to a json file which can then be further processed from the c++ side. The file network_evaluation.py can be used to check the accuracy of the pruned network

## Setup
To install the required libraries for cpu-only (maybe after creating a virtual environment):

    pip install numpy tensorflow-cpu

