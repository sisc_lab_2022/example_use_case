import tensorflow as tf
import keras
import numpy as np
import json


def extract_strings(lst):
    string_elements = []
    for i in lst:
        if isinstance(i, list):
            string_elements.extend(extract_strings(i))
        elif isinstance(i, str):
            string_elements.append(i)
    return string_elements


def activation_json_from_obj(activation):
    if activation is tf.keras.activations.sigmoid:
        return {"type": "sigmoid"}
    elif activation is tf.keras.activations.linear:
        return {"type": "linear"}
    elif activation is tf.keras.activations.relu:
        return {"type": "relu"}
    else:
        raise NotImplementedError(
            f"Unknown activation function {activation}")

def activation_obj_from_json(activation):
    if activation["type"] == "sigmoid":
        return "sigmoid"
    elif activation["type"] == "linear":
        return "linear"
    elif activation["type"] == "relu":
        return "relu"
    else:
        raise NotImplementedError(
            "Unknown activation function " + activation["type"])


def model_to_json(model, path):

    model_dict = {}
    model_tf_json = json.loads(model.to_json())

    if type(model) == keras.engine.functional.Functional:

        model_dict["type"] = "functional"

        input_layers = extract_strings(model_tf_json["config"]["input_layers"])
        assert(len(input_layers) == 1)
        model_dict["input_layer"] = input_layers[0]

        output_layers = extract_strings(model_tf_json["config"]["output_layers"])
        assert(len(output_layers) == 1)
        model_dict["output_layer"] = output_layers[0]

        layers = {}

        for layer, layer_json in zip(model.layers, model_tf_json["config"]["layers"]):
            layer_dict = {}
            layer_dict["name"] = layer.name
            layer_dict["input_nodes"] = extract_strings(layer_json["inbound_nodes"])

            if type(layer) == keras.engine.input_layer.InputLayer:
                layer_dict["type"] = "input"
                assert(len(layer.input_shape) == 1)
                layer_dict["input_shape"] = layer.input_shape[0][1:]

            elif type(layer) == tf.keras.layers.Dense:
                layer_dict["type"] = "dense"
                layer_dict["activation"] = activation_json_from_obj(layer.activation)

                # weights are transposed because of tfs weired format
                layer_dict["weights"] = layer.get_weights()[0].T.tolist()
                layer_dict["biases"] = layer.get_weights()[1].tolist()

            elif type(layer) == tf.keras.layers.Conv2D:
                layer_dict["type"] = "conv2d"
                layer_dict["activation"] = activation_json_from_obj(layer.activation)

                layer_dict["weights"] = layer.get_weights()[0].tolist()
                layer_dict["biases"] = layer.get_weights()[1].tolist()

                layer_dict["input_shape"] = layer.input_shape[1:]
                layer_dict["padding"] = layer.padding
                layer_dict["strides"] = layer.strides

            elif type(layer) == tf.keras.layers.ZeroPadding2D:
                layer_dict["type"] = "zeropadding2d"
                layer_dict["input_shape"] = layer.input_shape[1:]
                padding = layer.padding
                layer_dict["top_padding"] = padding[0][0]
                layer_dict["bottom_padding"] = padding[0][1]
                layer_dict["left_padding"] = padding[1][0]
                layer_dict["right_padding"] = padding[1][1]

            elif type(layer) == tf.keras.layers.Flatten:
                layer_dict["type"] = "flatten"
                layer_dict["input_shape"] = layer.input_shape[1:]

            elif type(layer) == tf.keras.layers.Activation:
                layer_dict["type"] = "activation"
                input_shape = layer.input_shape[1:]
                if len(input_shape) != 3:
                    raise Exception("An activation layer must have a 3d input shape. Activation layer after Dense layer not allowed")
                layer_dict["input_shape"] = input_shape
                layer_dict["activation"] = activation_json_from_obj(layer.activation)

            elif type(layer) == tf.keras.layers.Add:
                layer_dict["type"] = "add"
                input_shape = layer.input_shape[0][1:]
                if len(input_shape) == 1: input_shape = (1,1) + input_shape
                elif len(input_shape) != 3: raise Exception("input into Add layer must be 1D or 3D")
                layer_dict["input_shape"] = input_shape

            elif type(layer) == tf.keras.layers.BatchNormalization:
                layer_dict["type"] = "batchnormalization"
                layer_dict["epsilon"] = layer.epsilon
                layer_dict["gamma"] = np.array(layer.gamma).tolist()
                layer_dict["beta"] = np.array(layer.beta).tolist()
                layer_dict["mean"] = np.array(layer.moving_mean).tolist()
                layer_dict["var"] = np.array(layer.moving_variance).tolist()
                layer_dict["momentum"] = layer.momentum
                layer_dict["input_shape"] = layer.input_shape[1:]

            elif type(layer) == tf.keras.layers.MaxPool2D:
                layer_dict["type"] = "maxpool2d"
                layer_dict["padding"] = layer.padding
                layer_dict["pool_size"] = layer.pool_size
                layer_dict["strides"] = layer.strides
                layer_dict["input_shape"] = layer.input_shape[1:]

            elif type(layer) == tf.keras.layers.AveragePooling2D:
                layer_dict["type"] = "avrgpool2d"
                layer_dict["padding"] = layer.padding
                layer_dict["pool_size"] = layer.pool_size
                layer_dict["strides"] = layer.strides
                layer_dict["input_shape"] = layer.input_shape[1:]

            elif type(layer) == tf.keras.layers.GlobalAveragePooling2D:
                layer_dict["type"] = "globalavrgpool2d"
                layer_dict["input_shape"] = layer.input_shape[1:]

            else:
                raise NotImplementedError(f"Unknown layer {type(layer)}")

            layers[layer.name] = layer_dict

        model_dict["layers"] = layers

    else:
        raise NotImplementedError(f"Unknown network type {type(model)}")

    with open(path, 'w') as f:
        json.dump(model_dict, f, indent=2)



def model_from_json(path):

    with open(path, 'r') as f:
        model_dict = json.load(f)

    assert(model_dict["type"] == "functional")

    input_layer = model_dict["input_layer"]
    output_layer = model_dict["output_layer"]

    for layer_name in model_dict["layers"]:

        layer_dict = model_dict["layers"][layer_name]

        if layer_dict["type"] == "input":
            layer = tf.keras.layers.Input(layer_dict["input_shape"], name=layer_name)

        elif layer_dict["type"] == "dense":

            # weights are transposed because of tfs weired format
            weights = np.array(layer_dict["weights"]).T
            biases = np.array(layer_dict["biases"])

            activation = activation_obj_from_json(layer_dict["activation"])

            layer = tf.keras.layers.Dense(units=weights.shape[1],
                                            activation=activation,
                                            name=layer_name)

            # build necessary to initialize weights
            layer.build(input_shape=(weights.shape[0],))
            layer.set_weights([weights, biases])

        elif layer_dict["type"] == "conv2d":
            weights = np.array(layer_dict["weights"])
            biases = np.array(layer_dict["biases"])
            activation = activation_obj_from_json(layer_dict["activation"])

            layer = tf.keras.layers.Conv2D(
                len(biases),
                weights.shape[:2],
                padding=layer_dict["padding"],
                strides=layer_dict["strides"],
                activation=activation,
                input_shape=layer_dict["input_shape"],
                name=layer_name
            )

            # build necessary to initialize weights
            layer.build(input_shape=layer_dict["input_shape"])
            layer.set_weights([weights, biases])

        elif layer_dict["type"] == "maxpool2d":
            layer = tf.keras.layers.MaxPool2D(
                pool_size=layer_dict["pool_size"],
                strides=layer_dict["strides"],
                padding=layer_dict["padding"],
                name=layer_name
            )

        elif layer_dict["type"] == "avrgpool2d":
            layer = tf.keras.layers.AveragePooling2D(
                pool_size=layer_dict["pool_size"],
                strides=layer_dict["strides"],
                padding=layer_dict["padding"],
                name=layer_name
            )

        elif layer_dict["type"] == "globalavrgpool2d":
            layer = tf.keras.layers.GlobalAveragePooling2D(name=layer_name)

        elif layer_dict["type"] == "zeropadding2d":
            layer = tf.keras.layers.ZeroPadding2D(
                (
                    (layer_dict["top_padding"], layer_dict["bottom_padding"]),
                    (layer_dict["left_padding"], layer_dict["right_padding"])
                ),
                input_shape=layer_dict["input_shape"],
                name=layer_name
            )

        elif layer_dict["type"] == "flatten":
            layer = tf.keras.layers.Flatten(name=layer_name)

        elif layer_dict["type"] == "activation":
            activation = activation_obj_from_json(layer_dict["activation"])
            layer = tf.keras.layers.Activation(activation, name=layer_name)

        elif layer_dict["type"] == "batchnormalization":
            layer = tf.keras.layers.BatchNormalization(
                momentum=layer_dict["momentum"],
                epsilon=layer_dict["epsilon"],
                name=layer_name
            )
            layer.build(input_shape=[None] + layer_dict["input_shape"])
            mean = np.array(layer_dict["mean"])
            var = np.array(layer_dict["var"])
            beta = np.array(layer_dict["beta"])
            gamma = np.array(layer_dict["gamma"])
            layer.set_weights([gamma, beta, mean, var])

        elif layer_dict["type"] == "add":
            layer = tf.keras.layers.Add(name=layer_name)

        else:
            raise NotImplementedError(
                "Unknown layer " + layer_dict["type"])

        layer_dict["instance"] = layer
        layer_dict["evaluated"] = False

    def get_output(layer_name):
        if layer_name == input_layer:
            return model_dict["layers"][layer_name]["instance"]
        elif model_dict["layers"][layer_name]["evaluated"]:
            return model_dict["layers"][layer_name]["value"]
        elif model_dict["layers"][layer_name]["type"] == "add":
            model_dict["layers"][layer_name]["value"] = model_dict["layers"][layer_name]["instance"](
                [get_output(input_node) for input_node in model_dict["layers"][layer_name]["input_nodes"]]
            )
            model_dict["layers"][layer_name]["evaluated"] = True
            return model_dict["layers"][layer_name]["value"]
        else: #only for non add layer
            assert(len(model_dict["layers"][layer_name]["input_nodes"]) == 1)
            model_dict["layers"][layer_name]["value"] = model_dict["layers"][layer_name]["instance"](
                get_output(model_dict["layers"][layer_name]["input_nodes"][0])
            )
            model_dict["layers"][layer_name]["evaluated"] = True
            return model_dict["layers"][layer_name]["value"]
    
    output = get_output(output_layer)
    inp = get_output(input_layer)

    model = tf.keras.Model(inputs=inp, outputs=output)

    return model