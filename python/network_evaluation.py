import tensorflow as tf
from network_io import model_to_json, model_from_json
import numpy as np


mnist = tf.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train = x_train/255
x_test = x_test/255


def calc_loss_accuracy(model_path):

    model = model_from_json(model_path)
    model.build(input_shape=(None,28,28))
    model.compile(
        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
    )

    loss, accuracy = model.evaluate(x_test, y_test, verbose=0)
    return loss, accuracy


loss, acc = calc_loss_accuracy("simple_network.json")
print("Accuracy", acc)