import tensorflow as tf
from network_io import model_to_json, model_from_json
import numpy as np


mnist = tf.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train = x_train/255
x_test = x_test/255



X_input = tf.keras.layers.Input((28,28,1))
X = tf.keras.layers.Conv2D(16, kernel_size=(5,5), activation="sigmoid")(X_input)
X = tf.keras.layers.MaxPool2D(pool_size=(
        2, 2), strides=(2, 2), padding="valid")(X)
X = tf.keras.layers.Conv2D(32, (5, 5), strides=(
        1, 1), padding="valid", activation="sigmoid")(X)
X = tf.keras.layers.MaxPool2D(pool_size=(
        2, 2), strides=(2, 2), padding="valid")(X)
X = tf.keras.layers.Flatten()(X)
X = tf.keras.layers.Dense(64, activation="sigmoid")(X)
X = tf.keras.layers.Dense(10)(X)
model = tf.keras.Model(inputs=X_input, outputs=X)


model.compile(
    optimizer=tf.keras.optimizers.Adam(0.001),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

model.fit(
    x_train,
    y_train,
    epochs=2,
    validation_data=(x_test,y_test)
)


model_to_json(model, "simple_network.json")

tf.keras.utils.plot_model(model, to_file='simple_network.png', show_shapes=True, show_layer_names=True)
